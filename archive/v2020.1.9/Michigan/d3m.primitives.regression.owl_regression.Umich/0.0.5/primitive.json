{
    "id": "fab94f96-74f4-4987-ab45-2a992cb37e0b",
    "version": "0.0.5",
    "name": "OWLRegression",
    "description": "Primitive for linear regression with OWL regularization.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "keywords": [
        "regularization",
        "regression",
        "supervised",
        "ordered weight L1",
        "proximal gradient descent"
    ],
    "source": {
        "name": "Michigan",
        "contact": "mailto:alsoltan@umich.edu",
        "uris": [
            "https://github.com/dvdmjohnson/d3m_michigan_primitives/blob/master/spider/supervised_learning/owl/owl.py",
            "https://github.com/dvdmjohnson/d3m_michigan_primitives"
        ],
        "citation": "@ARTICLE{2014arXiv1407.3824B,\n                author = {{Bogdan}, M. and {van den Berg}, E. and {Sabatti}, C. and {Su}, W. and \n                {Cand{\\`e}s}, E.~J.},\n                title = \"{SLOPE - Adaptive variable selection via convex optimization}\",\n                journal = {ArXiv e-prints},\n                archivePrefix = \"arXiv\",\n                eprint = {1407.3824},\n                primaryClass = \"stat.ME\",\n                keywords = {Statistics - Methodology},\n                year = 2014,\n                month = jul,\n                adsurl = {http://adsabs.harvard.edu/abs/2014arXiv1407.3824B},\n                adsnote = {Provided by the SAO/NASA Astrophysics Data System}\n                } "
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/dvdmjohnson/d3m_michigan_primitives.git@d040be3fc7070669a081df53d9e5117a2349234d#egg=spider"
        },
        {
            "type": "UBUNTU",
            "package": "ffmpeg",
            "version": "7:2.8.11-0ubuntu0.16.04.1"
        }
    ],
    "python_path": "d3m.primitives.regression.owl_regression.Umich",
    "hyperparams_to_tune": [
        "weight_type",
        "weight_max_val",
        "weight_max_off",
        "weight_min_val",
        "weight_min_off",
        "fit_intercept",
        "normalize",
        "tol",
        "learning_rate",
        "verbose"
    ],
    "algorithm_types": [
        "LINEAR_REGRESSION"
    ],
    "primitive_family": "REGRESSION",
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "spider.supervised_learning.owl.owl.OWLRegression",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.numpy.ndarray",
            "Outputs": "d3m.container.numpy.ndarray",
            "Params": "spider.supervised_learning.owl.owl.OWLParams",
            "Hyperparams": "spider.supervised_learning.owl.owl.OWLHyperparams"
        },
        "interfaces_version": "2020.1.9",
        "interfaces": [
            "supervised_learning.SupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "weight_type": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "linear",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "type of hyperparameters. future: quadratic, logarithm",
                "values": [
                    "linear"
                ]
            },
            "weight_max_val": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0.01,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "should be greater than weight_min_val",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "weight_max_off": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "should be smaller than weight_min_off and n",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "weight_min_val": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0.01,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "should be no greater than weight_max_val (or equal to, when the two cutoff values are the same)",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "weight_min_off": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "should be greater than or equal to weight_max_off and smaller than n",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "fit_intercept": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": true,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether the intercept should be estimated"
            },
            "normalize": {
                "type": "d3m.metadata.hyperparams.UniformBool",
                "default": false,
                "structural_type": "bool",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "Whether the features should be centerized and standardized. Ignored when fit_intercept is set to False"
            },
            "tol": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0.001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "tolerance to exit iterations",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "learning_rate": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0.001,
                "structural_type": "float",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "learning rate of proximal gradient descent (FISTA)",
                "lower": 0,
                "upper": null,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "verbose": {
                "type": "d3m.metadata.hyperparams.Bounded",
                "default": 0,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/ControlParameter"
                ],
                "description": "0: no training output. 1: exit status. 2: every iteration",
                "lower": 0,
                "upper": 2,
                "lower_inclusive": true,
                "upper_inclusive": true
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "spider.supervised_learning.owl.owl.OWLHyperparams",
                "kind": "RUNTIME"
            },
            "random_seed": {
                "type": "int",
                "kind": "RUNTIME",
                "default": 0
            },
            "docker_containers": {
                "type": "typing.Union[NoneType, typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]]",
                "kind": "RUNTIME",
                "default": null
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "outputs": {
                "type": "d3m.container.numpy.ndarray",
                "kind": "PIPELINE"
            },
            "params": {
                "type": "spider.supervised_learning.owl.owl.OWLParams",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "spider.supervised_learning.owl.owl.OWLHyperparams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]"
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams",
                    "random_seed",
                    "docker_containers"
                ],
                "returns": "NoneType"
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Fit the linear regression problem with OWL regularization\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "outputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nThis method allows primitive author to implement an optimized version of both fitting\nand producing a primitive on same data.\n\nIf any additional method arguments are added to primitive's ``set_training_data`` method\nor produce method(s), or removed from them, they have to be added to or removed from this\nmethod as well. This method should accept an union of all arguments accepted by primitive's\n``set_training_data`` method and produce method(s) and then use them accordingly when\ncomputing results.\n\nThe default implementation of this method just calls first ``set_training_data`` method,\n``fit`` method, and all produce methods listed in ``produce_methods`` in order and is\npotentially inefficient.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\noutputs : Outputs\n    The outputs given to ``set_training_data``.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "spider.supervised_learning.owl.owl.OWLParams",
                "description": "Returns parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nParameters should include all data which is necessary to create a new instance of this primitive\nbehaving exactly the same as this instance, when the new instance is created by passing the same\nparameters to the class constructor and calling ``set_params``.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nReturns\n-------\nParams\n    An instance of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.numpy.ndarray]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Compute the predictions given inputs with shape n by m,\nyielding an array of size n.\n\nInputs must match the dimensionality of the training data.\n\nParameters\n----------\ninputs : Inputs\n    The inputs of shape [num_inputs, ...].\ntimeout : float\n    A maximum time this primitive should take to produce outputs during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[Outputs]\n    The outputs of shape [num_inputs, ...] wrapped inside ``CallResult``."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Sets parameters of this primitive.\n\nParameters are all parameters of the primitive which can potentially change during a life-time of\na primitive. Parameters which cannot are passed through constructor.\n\nNo other arguments to the method are allowed (except for private arguments).\n\nParameters\n----------\nparams : Params\n    An instance of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs",
                    "outputs"
                ],
                "returns": "NoneType",
                "description": "Arguments:\n    inputs: nSamples x nFeatures\n    outputs: nSamples, or nSamples x 1, ... (All dim after the first dim should have length 1)\n\nParameters\n----------\ninputs : Inputs\n    The inputs.\noutputs : Outputs\n    The outputs."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "fitted": "bool",
            "coef": "d3m.container.numpy.ndarray",
            "intercept": "float"
        }
    },
    "structural_type": "spider.supervised_learning.owl.owl.OWLRegression",
    "digest": "d7a387bc2f4860129e5ef7e98a4ef6e6bc0ef0de68bda0cbef95847ab1e2d465"
}
