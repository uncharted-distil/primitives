{
    "id": "49af9397-d9a2-450f-93eb-c3b631ba6646",
    "version": "3.0.1",
    "name": "Gaussian Mixture Models",
    "description": "This class provides functionality for unsupervised inference on Gaussian mixture model, which is a probabilistic\nmodel that assumes all the data points are generated from a mixture of a finite number of Gaussian distributions\nwith unknown parameters. It can be viewed as a generalization of the K-Means clustering to incorporate\ninformation about the covariance structure of the data. Standard packages, like those in scikit learn run on a\nsingle machine and often only on one thread. Whereas our underlying C++ implementation can be distributed to run\non multiple machines. To enable the distribution through python interface is work in progress. In this class,\nwe implement inference on (Bayesian) Gaussian mixture models using Canopy algorithm. The API is similar to\nsklearn.mixture.GaussianMixture. The class is pickle-able.\n\nAttributes\n----------\nmetadata : PrimitiveMetadata\n    Primitive's metadata. Available as a class attribute.\nlogger : Logger\n    Primitive's logger. Available as a class attribute.\nhyperparams : Hyperparams\n    Hyperparams passed to the constructor.\nrandom_seed : int\n    Random seed passed to the constructor.\ndocker_containers : Dict[str, DockerContainer]\n    A dict mapping Docker image keys from primitive's metadata to (named) tuples containing\n    container's address under which the container is accessible by the primitive, and a\n    dict mapping exposed ports to ports on that address.\nvolumes : Dict[str, str]\n    A dict mapping volume keys from primitive's metadata to file and directory paths\n    where downloaded and extracted files are available to the primitive.\ntemporary_directory : str\n    An absolute path to a temporary directory a primitive can use to store any files\n    for the duration of the current pipeline run phase. Directory is automatically\n    cleaned up after the current pipeline run phase finishes.",
    "python_path": "d3m.primitives.clustering.gmm.Fastlvm",
    "primitive_family": "CLUSTERING",
    "algorithm_types": [
        "K_MEANS_CLUSTERING"
    ],
    "keywords": [
        "large scale Gaussian Mixture Models",
        "clustering"
    ],
    "source": {
        "name": "CMU",
        "contact": "mailto:donghanw@cs.cmu.edu",
        "uris": [
            "https://gitlab.datadrivendiscovery.org/cmu/fastlvm",
            "https://github.com/autonlab/fastlvm"
        ]
    },
    "installation": [
        {
            "type": "PIP",
            "package_uri": "git+https://github.com/autonlab/fastlvm.git@b81edbb36a15e5c969498fac5dfd2abf336ee2ba#egg=fastlvm"
        }
    ],
    "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/primitive.json",
    "original_python_path": "fastlvm.gmm.GMM",
    "primitive_code": {
        "class_type_arguments": {
            "Inputs": "d3m.container.pandas.DataFrame",
            "Outputs": "d3m.container.pandas.DataFrame",
            "Params": "fastlvm.gmm.Params",
            "Hyperparams": "fastlvm.gmm.HyperParams"
        },
        "interfaces_version": "2019.5.8",
        "interfaces": [
            "unsupervised_learning.UnsupervisedLearnerPrimitiveBase",
            "base.PrimitiveBase"
        ],
        "hyperparams": {
            "k": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 10,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "The number of clusters to form as well as the number of centroids to generate.",
                "lower": 1,
                "upper": 10000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "iters": {
                "type": "d3m.metadata.hyperparams.UniformInt",
                "default": 100,
                "structural_type": "int",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "The number of iterations of inference.",
                "lower": 1,
                "upper": 10000,
                "lower_inclusive": true,
                "upper_inclusive": false
            },
            "initialization": {
                "type": "d3m.metadata.hyperparams.Enumeration",
                "default": "covertree",
                "structural_type": "str",
                "semantic_types": [
                    "https://metadata.datadrivendiscovery.org/types/TuningParameter"
                ],
                "description": "'random': choose k observations (rows) at random from data for the initial centroids. 'kmeanspp' : selects initial cluster centers by finding well spread out points using cover trees to speed up convergence. 'covertree' : selects initial cluster centers by sampling to speed up convergence.",
                "values": [
                    "random",
                    "firstk",
                    "kmeanspp",
                    "covertree"
                ]
            }
        },
        "arguments": {
            "hyperparams": {
                "type": "fastlvm.gmm.HyperParams",
                "kind": "RUNTIME"
            },
            "inputs": {
                "type": "d3m.container.pandas.DataFrame",
                "kind": "PIPELINE"
            },
            "timeout": {
                "type": "typing.Union[NoneType, float]",
                "kind": "RUNTIME",
                "default": null
            },
            "iterations": {
                "type": "typing.Union[NoneType, int]",
                "kind": "RUNTIME",
                "default": null
            },
            "produce_methods": {
                "type": "typing.Sequence[str]",
                "kind": "RUNTIME"
            },
            "params": {
                "type": "fastlvm.gmm.Params",
                "kind": "RUNTIME"
            }
        },
        "class_methods": {
            "can_accept": {
                "arguments": {
                    "method_name": {
                        "type": "str"
                    },
                    "arguments": {
                        "type": "typing.Dict[str, typing.Union[d3m.metadata.base.Metadata, type]]"
                    },
                    "hyperparams": {
                        "type": "fastlvm.gmm.HyperParams"
                    }
                },
                "returns": "typing.Union[NoneType, d3m.metadata.base.DataMetadata]",
                "description": "Returns a metadata object describing the output of a call of ``method_name`` method under\n``hyperparams`` with primitive arguments ``arguments``, if such arguments can be accepted by the method.\nOtherwise it returns ``None`` or raises an exception.\n\nDefault implementation checks structural types of ``arguments`` expected arguments' types\nand ignores ``hyperparams``.\n\nBy (re)implementing this method, a primitive can fine-tune which arguments it accepts\nfor its methods which goes beyond just structural type checking. For example, a primitive might\noperate only on images, so it can accept numpy arrays, but only those with semantic type\ncorresponding to an image. Or it might check dimensions of an array to assure it operates\non square matrix.\n\nPrimitive arguments are a superset of method arguments. This method receives primitive arguments and\nnot just method arguments so that it is possible to implement it without a state between calls\nto ``can_accept`` for multiple methods. For example, a call to ``fit`` could during normal execution\ninfluences what a later ``produce`` call outputs. But during ``can_accept`` call we can directly have\naccess to arguments which would have been given to ``fit`` to produce metadata of the ``produce`` call.\n\nNot all primitive arguments have to be provided, only those used by ``fit``, ``set_training_data``,\nand produce methods, and those used by the ``method_name`` method itself.\n\nParameters\n----------\nmethod_name : str\n    Name of the method which would be called.\narguments : Dict[str, Union[Metadata, type]]\n    A mapping between argument names and their metadata objects (for pipeline arguments) or types (for other).\nhyperparams : Hyperparams\n    Hyper-parameters under which the method would be called during regular primitive execution.\n\nReturns\n-------\nDataMetadata\n    Metadata object of the method call result, or ``None`` if arguments are not accepted\n    by the method."
            }
        },
        "instance_methods": {
            "__init__": {
                "kind": "OTHER",
                "arguments": [
                    "hyperparams"
                ],
                "returns": "NoneType"
            },
            "evaluate": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "float",
                "description": "Finds the score of learned model on a set of test points\n\nParameters\n----------\ninputs : Inputs\n    A NxD DataFrame of data points.\n\nReturns\n-------\nscore : float\n    The log-likelihood on the supplied points."
            },
            "fit": {
                "kind": "OTHER",
                "arguments": [
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[NoneType]",
                "description": "Inference on the Gaussian mixture model\n\nParameters\n----------\ntimeout : float\n    A maximum time this primitive should be fitting during this method call, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nCallResult[None]\n    A ``CallResult`` with ``None`` value."
            },
            "fit_multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling ``fit`` and after that multiple produce methods at once.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to ``set_training_data`` and all produce methods.\ntimeout : float\n    A maximum time this primitive should take to both fit the primitive and produce outputs\n    for all produce methods listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do for both fitting and producing\n    outputs of all produce methods.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "get_call_metadata": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "bool",
                "description": "Returns metadata about the last ``fit`` call if it succeeded\n\nReturns\n-------\nStatus : bool\n    True/false status of fitting."
            },
            "get_params": {
                "kind": "OTHER",
                "arguments": [],
                "returns": "fastlvm.gmm.Params",
                "description": "Get parameters of GMM.\n\nParameters are basically the mixture parameters (mean and variance) in byte stream.\n\nReturns\n-------\nparams : Params\n    A named tuple of parameters."
            },
            "multi_produce": {
                "kind": "OTHER",
                "arguments": [
                    "produce_methods",
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.MultiCallResult",
                "description": "A method calling multiple produce methods at once.\n\nWhen a primitive has multiple produce methods it is common that they might compute the\nsame internal results for same inputs but return different representations of those results.\nIf caller is interested in multiple of those representations, calling multiple produce\nmethods might lead to recomputing same internal results multiple times. To address this,\nthis method allows primitive author to implement an optimized version which computes\ninternal results only once for multiple calls of produce methods, but return those different\nrepresentations.\n\nIf any additional method arguments are added to primitive's produce method(s), they have\nto be added to this method as well. This method should accept an union of all arguments\naccepted by primitive's produce method(s) and then use them accordingly when computing\nresults.\n\nThe default implementation of this method just calls all produce methods listed in\n``produce_methods`` in order and is potentially inefficient.\n\nIf primitive should have been fitted before calling this method, but it has not been,\nprimitive should raise a ``PrimitiveNotFittedError`` exception.\n\nParameters\n----------\nproduce_methods : Sequence[str]\n    A list of names of produce methods to call.\ninputs : Inputs\n    The inputs given to all produce methods.\ntimeout : float\n    A maximum time this primitive should take to produce outputs for all produce methods\n    listed in ``produce_methods`` argument, in seconds.\niterations : int\n    How many of internal iterations should the primitive do.\n\nReturns\n-------\nMultiCallResult\n    A dict of values for each produce method wrapped inside ``MultiCallResult``."
            },
            "produce": {
                "kind": "PRODUCE",
                "arguments": [
                    "inputs",
                    "timeout",
                    "iterations"
                ],
                "returns": "d3m.primitive_interfaces.base.CallResult[d3m.container.pandas.DataFrame]",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Finds the closest cluster for the given set of test points using the learned model.\n\nParameters\n----------\ninputs : Inputs\n    A NxD DataFrame of data points.\n\nReturns\n-------\nOutputs\n    The index of the cluster each sample belongs to."
            },
            "produce_centers": {
                "kind": "PRODUCE",
                "arguments": [],
                "returns": "d3m.container.numpy.ndarray",
                "singleton": false,
                "inputs_across_samples": [],
                "description": "Get current cluster means and variances for this model.\n\nReturns\n----------\nmeans : numpy.ndarray\n    A KxD matrix of cluster means.\nvars : numpy.ndarray\n    A KxD matrix of cluster variances."
            },
            "set_params": {
                "kind": "OTHER",
                "arguments": [
                    "params"
                ],
                "returns": "NoneType",
                "description": "Set parameters of GMM.\n\nParameters are basically the mixture parameters (mean and variance) in byte stream.\n\nParameters\n----------\nparams : Params\n    A named tuple of parameters."
            },
            "set_training_data": {
                "kind": "OTHER",
                "arguments": [
                    "inputs"
                ],
                "returns": "NoneType",
                "description": "Sets training data for GMM.\n\nParameters\n----------\ntraining_inputs : Inputs\n    A NxD DataFrame of data points for training."
            }
        },
        "class_attributes": {
            "logger": "logging.Logger",
            "metadata": "d3m.metadata.base.PrimitiveMetadata"
        },
        "instance_attributes": {
            "hyperparams": "d3m.metadata.hyperparams.Hyperparams",
            "random_seed": "int",
            "docker_containers": "typing.Dict[str, d3m.primitive_interfaces.base.DockerContainer]",
            "volumes": "typing.Dict[str, str]",
            "temporary_directory": "typing.Union[NoneType, str]"
        },
        "params": {
            "mixture_parameters": "bytes"
        }
    },
    "structural_type": "fastlvm.gmm.GMM",
    "digest": "b31cbfa5cf56b62c801b4b8612defa25717283012e666353f1b21a054c5ffb1d"
}
